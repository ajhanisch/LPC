#!/usr/env/python
# -*- coding: utf-8 -*-

import os
import sys
import socket
import smtplib
import datetime
from time import strftime
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart

'''
CLASSES
'''
class Email:

	def __init__(self, **kwargs):
		for key, value in kwargs.items():
			setattr(self, key, value)

	def send_email(self):
		try:
			if self.attachment:
				file_open = open(self.attachment, 'rb') # Open a plain text file for reading.  For this example, assume that the text file contains only ASCII characters.
				message = MIMEText(file_open.read()) # Create a text/plain message
				file_open.close()
				
				message['Subject'] = self.subject
				message['From'] = self.send_from
				message['To'] = self.send_to
				
				s = smtplib.SMTP(self.server_smtp)
				s.sendmail(self.send_from, self.send_to, message.as_string())
				s.quit()
		except AttributeError:
			message = MIMEMultipart()
			message['Subject'] = self.subject
			message['From'] = self.send_from
			message['To'] = self.send_to
			
			s = smtplib.SMTP(self.server_smtp)
			s.sendmail(self.send_from, self.send_to, message.as_string())
			s.quit()

class Setup:
	'''
	LISTS
	'''
	list_user_information = []

	'''
	VARIABLES
	'''
	version = '0.4'
	machine = socket.gethostname()
	program = os.path.basename(__file__)
	date = strftime('%Y-%m-%d_%H-%M-%S')

	'''
	DIRECTORIES
	'''
	directory_working = os.getcwd()
	directory_working_log = os.path.join(directory_working, 'LOGS', date)
	directory_automation = '/opt/local'

	'''
	FILES
	'''
	file_shadow = '/etc/shadow'
	file_users_expired = os.path.join(directory_working_log, 'users_expired.log')
	file_users_fifteen = os.path.join(directory_working_log, 'users_fifteen.log')
	file_users_ten = os.path.join(directory_working_log, 'users_ten.log')

def main():
	'''
	Main function. Everything starts here.
	'''
	setup = Setup()
	
	'''
	Make sure we are on a Linux system.
	'''
	if sys.platform == 'linux' or sys.platform == 'linux2':
		pass
	else:
		print('WARNING! Running on {0} platform. {1} is designed for linux based systems only. Exiting now.'.format(sys.platform, setup.machine))
		exit()

	'''
	Create log directory.
	'''
	if not os.path.exists(setup.directory_working_log):
		os.makedirs(setup.directory_working_log)

	'''
	Check the /etc/shadow file to get a list of valid users on the box.
	'Valid user' is defined as a user or entry in the /etc/shadow file that has a hashed password. Hashed passwords will start with a $.
	'''
	with open(setup.file_shadow, 'r') as f:
		valid_users = [ x.split(':')[0] for x in f if '$' in x ]

	'''
	Check the /etc/shadow file to get password expiration information for list of valid users on box.
	'''
	for valid_user in valid_users:
		with open(setup.file_shadow, 'r') as f:
			valid_user_information = [ x for x in f if valid_user in x ]
			if valid_user_information:
				valid_user_information = str(valid_user_information)
				valid_user_information_last_changed = int(valid_user_information.split(":")[2])
				valid_user_information_required_change = int(valid_user_information.split(":")[4])
				days_epoch_now = (datetime.datetime.utcnow() - datetime.datetime(1970,1,1)).days
				days_since_last_changed = days_epoch_now - valid_user_information_last_changed
				dictionary_data = {
					'ACCOUNT' : valid_user,
					'MACHINE' : setup.machine,
					'LAST_CHANGED': days_since_last_changed,
					'REQUIRED_CHANGE': valid_user_information_required_change
				}
				setup.list_user_information.append(dictionary_data)
			else:
				'''
				We should never enter this conditional, but it is here just in case.
				'''
				print('Unable to find valid user information in {0} for {1}. This is a problem outside of this script. Please correct this and try again.'.format(setup.file_shadow, valid_user))

	'''
	Create lists of users based on criteria.
	'''
	list_users_expired = [ i for i in setup.list_user_information if i['LAST_CHANGED'] > i['REQUIRED_CHANGE'] ]
	list_users_fifteen = [ i for i in setup.list_user_information if i['LAST_CHANGED'] == i['REQUIRED_CHANGE'] - 15 ]
	list_users_ten_or_less = [ i for i in setup.list_user_information if i['LAST_CHANGED'] >= i['REQUIRED_CHANGE'] - 10 and i['LAST_CHANGED'] <= i['REQUIRED_CHANGE'] ]
	list_users_other = [ i for i in setup.list_user_information if i not in list_users_expired and i not in list_users_fifteen and i not in list_users_ten_or_less ]

	'''
	Send emails/reports to desired people/groups.
	'''
	if len(list_users_expired) > 0:
		with open(setup.file_users_expired, 'w') as outfile:
			for i in list_users_expired:
				'''
				Write result to log file for SA section.
				'''
				outfile.write('ACCOUNT: {0} || MACHINE: {1} || LAST_CHANGE: {2} days || REQUIRED_CHANGE: {3} days\n'.format(i['ACCOUNT'], i['MACHINE'], i['LAST_CHANGED'], i['REQUIRED_CHANGE']))

				'''
				Create and send email to individual user (i) notifying of required password change.
				'''
				dict_data = {
					'subject' : 'PASSWORD EXPIRED on {0}'.format(setup.machine),
					'send_from' : 'root',
					'send_to' : i['ACCOUNT'],
					'server_smtp' : 'localhost'
				}
				Email(**dict_data).send_email()

			'''
			Create and send email to SA section with file_users_expired.log.
			'''
			outfile.write('\nNote for SAs: This message and results from {0} on {1} within {2} directory.'.format(setup.program, setup.machine, setup.directory_automation))

		dict_data = {
			'attachment' : setup.file_users_expired,
			'subject' : 'EXPIRED USERS: {0} on {1}'.format(len(list_users_expired), setup.machine),
			'send_from' : 'root',
			'send_to' : 'root',
			'server_smtp' : 'localhost'
		}
		Email(**dict_data).send_email()

	if len(list_users_fifteen) > 0:
		with open(setup.file_users_fifteen, 'w') as outfile:
			for i in list_users_fifteen:
				'''
				Write result to log file for SA section.
				'''
				outfile.write('ACCOUNT: {0} || MACHINE: {1} || LAST_CHANGE: {2} days || REQUIRED_CHANGE: {3} days\n'.format(i['ACCOUNT'], i['MACHINE'], i['LAST_CHANGED'], i['REQUIRED_CHANGE']))
				
				'''
				Create and send email to individual user (i) notifying of required password change in 15 days.
				'''
				dict_data = {
					'subject' : 'PASSWORD EXPIRES IN 15 DAYS on {0}'.format(setup.machine),
					'send_from' : 'root',
					'send_to' : i['ACCOUNT'],
					'server_smtp' : 'localhost'
				}
				Email(**dict_data).send_email()

			'''
			Create and send email to SA section with users_fifteen.log.
			'''
			outfile.write('\nNote for SAs: This message and results from {0} on {1} within {2} directory.'.format(setup.program, setup.machine, setup.directory_automation))
			
		dict_data = {
			'attachment' : setup.file_users_fifteen,
			'subject' : 'PASSWORD EXPIRES IN 15 DAYS on {0}'.format(setup.machine),
			'send_from' : 'root',
			'send_to' : 'root',
			'server_smtp' : 'localhost'
		}
		Email(**dict_data).send_email()

	if len(list_users_ten_or_less) > 0:
		with open(setup.file_users_ten, 'w') as outfile:
			for i in list_users_ten_or_less:
				'''
				Write result to log file for SA section.
				'''
				outfile.write('ACCOUNT: {0} || MACHINE: {1} || LAST_CHANGE: {2} days || REQUIRED_CHANGE: {3} days\n'.format(i['ACCOUNT'], i['MACHINE'], i['LAST_CHANGED'], i['REQUIRED_CHANGE']))
				
				'''
				Create and send email to individual user (i) notifying of required password change in 10 days.
				'''
				dict_data = {
					'subject' : 'PASSWORD EXPIRES IN 10 DAYS on {0}'.format(setup.machine),
					'send_from' : 'root',
					'send_to' : i['ACCOUNT'],
					'server_smtp' : 'localhost'
				}
				Email(**dict_data).send_email()

			'''
			Create and send email to SA section with users_fifteen.log.
			'''
			outfile.write('\nNote for SAs: This message and results from {0} on {1} within {2} directory.'.format(setup.program, setup.machine, setup.directory_automation))

		dict_data = {
			'attachment' : setup.file_users_ten,
			'subject' : 'PASSWORD EXPIRES IN 10 DAYS on {0}'.format(setup.machine),
			'send_from' : 'root',
			'send_to' : 'root',
			'server_smtp' : 'localhost'
		}
		Email(**dict_data).send_email()

'''
Entry point of script.
'''
if __name__ == '__main__':
	main()